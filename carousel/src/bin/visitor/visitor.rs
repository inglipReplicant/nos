extern crate posixmq;
extern crate rand;

use std::env;
use std::{thread, time};

use carousel::{calc_mq_name, DONE, REQUEST, SIT, STAND};
use posixmq::OpenOptions;
use rand::{thread_rng, Rng};

fn main() {
    let args: Vec<String> = env::args().collect();
    let my_id = args[1].parse::<usize>().unwrap();
    let parent_id = args[2].parse::<usize>().unwrap(); // Parent id is used in MQ name
    println!("Hello world from visitor {}, parent {}!", my_id, parent_id);
    let mq_name = calc_mq_name(parent_id, my_id, 's');
    let mq_read = OpenOptions::readonly()
        .capacity(5)
        .max_msg_len(200)
        .open(&mq_name)
        .expect("Unable to create MQ from visitor");

    let mq_name = calc_mq_name(parent_id, my_id, 'c');
    let mq_write = OpenOptions::writeonly()
        .capacity(5)
        .max_msg_len(200)
        .open(&mq_name)
        .expect("Unable to create MQ from visitor");

    let mut msgbuf = [0; 200];
    for _ in 0..3 {
        let duration = thread_rng().gen_range(100, 2001);
        thread::sleep(time::Duration::from_millis(duration));
        mq_write
            .send(0, &REQUEST.as_bytes())
            .expect("Unable to send REQUEST to server");

        //println!("K: {} mq ima {} poruka", my_id, mq.attributes().current_messages);
        let (_, len) = mq_read
            .receive(&mut msgbuf)
            .expect("Unable to read SIT message");
        let msg = String::from_utf8_lossy(&msgbuf[..len]);
        if msg != SIT {
            panic!(
                "K: {} msg: {} not equal to SIT: {} . ABORTING!",
                my_id, msg, SIT
            );
        }
        println!("Sjeo posjetitelj {}", my_id);
        let (_, len) = mq_read
            .receive(&mut msgbuf)
            .expect("Unable to read STAND message");
        let msg = String::from_utf8_lossy(&msgbuf[..len]);
        if msg != STAND {
            panic!(
                "K: {} msg: {} not equal to STAND: {} . ABORTING!",
                my_id, msg, STAND
            );
        }
        println!("Ustao i sisao posjetitelj {}", my_id);
    }
    mq_write
        .send(0, &DONE.as_bytes())
        .expect("Unable to send DONE to server");
}
