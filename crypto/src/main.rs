#[macro_use]
extern crate clap;
extern crate openssl;

use clap::ArgMatches;

use openssl::{
    base64::{decode_block, encode_block},
    bn::{BigNum, MsbOption},
    hash::MessageDigest,
    pkcs5::bytes_to_key,
    pkey::{PKey, Private, Public},
    rsa::{Padding, Rsa},
    sign::{Signer, Verifier},
    symm::{decrypt, encrypt, Cipher},
};

use std::fs::File;
use std::io::prelude::*;
use std::str;

const ALG_AES: &str = "AES";
const ALG_BF: &str = "BF"; //Schneier's Blowfish

const SYM_128: &str = "128";
const SYM_192: &str = "192";
const SYM_256: &str = "256";

const MODE_ECB: &str = "ECB";
const MODE_CBC: &str = "CBC";
const MODE_CFB: &str = "CFB";
const MODE_OFB: &str = "OFB";
const MODE_CTR: &str = "CTR";

const RSA_1024: &str = "1024";
const RSA_2048: &str = "2048";
const RSA_3072: &str = "3072";
const RSA_4096: &str = "4096";

const HASH_MD5: &str = "MD5";
const HASH_SHA1: &str = "SHA1";
const HASH_SHA224: &str = "SHA224";
const HASH_SHA256: &str = "SHA256";
const HASH_SHA384: &str = "SHA384";
const HASH_SHA512: &str = "SHA512";
const HASH_SHA3_224: &str = "SHA3_224";
const HASH_SHA3_256: &str = "SHA3_256";
const HASH_SHA3_384: &str = "SHA3_384";
const HASH_SHA3_512: &str = "SHA3_512";

fn main() {
    openssl::init();

    let matches = clap_app!(crypto =>
        (version: "1.0")
        (author: "Moje ime i prezime")
        (about: "Tiny encryption and signing tool")
        (@arg SYMM_ALG: -s --symalg +takes_value "Symmetric encryption algorithm (default: AES)")
        (@arg SYMM_KEY: -k --symkey +takes_value "Symmetric algorithm key length (default: 256)")
        (@arg SYMM_MOD: -m --symmod +takes_value "Symmetric algorithm encryption mode (default: CBC)")
        (@arg ASYM_ALG: -a --asyalg +takes_value "Asymmetric algorithm (default: RSA)")
        (@arg ASYM_LEN: -l --asylen +takes_value "Asymmetric algorithm key length (default: 2048)")
        (@arg HASH_FUN: -h --hashfn +takes_value "Cryptographic hash function (default: SHA3_512)")
        (@subcommand gen =>
            (about: "Generates a public/private RSA key pair stored in two files.")
            (@arg S_KEY: +required "Output secret key file (PEM encoded PKCS #1).")
            (@arg P_KEY: +required "Output public key file (PEM encoded PKCS #8).")
        )
        (@subcommand sign =>
            (about: "Signs a message stored in a file.")
            (@arg S_KEY: +required "Path to my own secret key, stored in a file, PEM encoded PKCS #8.")
            (@arg MSG: +required "Path to the message file.")
        )
        (@subcommand verify =>
            (about: "Verifies a signed message stored in a file.")
            (@arg P_KEY: +required "Path to their public key, stored in a file, PEM encoded PKCS #1.")
            (@arg MSG: +required "Path to the signed message file.")
        )
        (@subcommand encrypt =>
            (about: "Encrypts a message stored in a file.")
            (@arg P_KEY: +required "Path to their public key, stored in a file, PEM encoded PKCS #1.")
            (@arg MSG: +required "Path to the plaintext file.")
        )
        (@subcommand decrypt =>
            (about: "Decrypts an encrypted message stored in a file.")
            (@arg S_KEY: +required "Path to my secret key, stored in a file, PEM encoded PKCS #8.")
            (@arg MSG: +required "Path to the ciphertext file.")
        )
        (@subcommand seal =>
            (about: "Seals a message stored in a file.")
            (@arg S_KEY: +required "Path to my secret key, stored in a file, PEM encoded PKCS #8.")
            (@arg P_KEY: +required "Path to their public key, stored in a file, PEM encoded PKCS #1.")
            (@arg MSG: +required "Path to the plaintext file.")
        )
        (@subcommand unseal =>
            (about: "Unseals a sealed message stored in a file.")
            (@arg S_KEY: +required "Path to my secret key, stored in a file, PEM encoded PKCS #8.")
            (@arg P_KEY: +required "Path to their public key, stored in a file, PEM encoded PKCS #1.")
            (@arg MSG: +required "Path to the ciphertext file.")
        )
    ).get_matches();

    let asym_key_len = parse_asym_key_len(&matches);

    // ako je naredba za generiranje, obavi posao i zavrsi program
    if let Some(matches) = matches.subcommand_matches("gen") {
        let (skey_path, pkey_path) = (
            matches.value_of("S_KEY").unwrap(),
            matches.value_of("P_KEY").unwrap(),
        );
        println!(
            "Generating RSA keypair (len: {}) | skey_path: {} | pkey_path: {}",
            asym_key_len, skey_path, pkey_path
        );
        generate_keys(asym_key_len, &skey_path, &pkey_path);
        return;
    }

    let digest_fn = parse_hash_fun(&matches);
    if let Some(matches) = matches.subcommand_matches("sign") {
        let my_sec = read_rsa_seckey(matches);
        let my_key = PKey::from_rsa(my_sec).unwrap();

        let message_path = matches.value_of("MSG").unwrap();
        let mut message = read_from_file(&message_path);
        message.pop();

        let signature = sign_bytes(&message, &my_key, digest_fn);
        let signed_message = concat_message_signature(&message, &signature);
        println!("{}", &signed_message);
        return;
    } else if let Some(matches) = matches.subcommand_matches("verify") {
        let their_pub = read_rsa_pubkey(matches);
        let their_pub = PKey::from_rsa(their_pub).unwrap();

        let message_path = matches.value_of("MSG").unwrap();

        let text = read_from_file(&message_path);
        let (text, b64_sig) = parse_signed_message(&text);
        //println!("{:?}", text);
        //println!("{:?}", b64_sig);

        let signature_bytes = decode_block(&b64_sig).expect("Unable to decode signature bytes.");
        let valid = verify_signature(&text.as_bytes(), &their_pub, &signature_bytes, digest_fn);
        if valid {
            println!("Verified OK!");
        } else {
            println!("Verification failed.");
        }
        return;
    }

    let symm_alg = parse_symm_alg(&matches);
    let symm_len = parse_symm_len(&matches);
    let symm_mode = parse_symm_mode(&matches);
    let cipher = create_cipher(symm_alg, symm_len, symm_mode);

    // ENCRYPT naredba
    if let Some(matches) = matches.subcommand_matches("encrypt") {
        let their_pub = read_rsa_pubkey(matches);

        let message_path = matches.value_of("MSG").unwrap();
        let mut text = read_from_file(&message_path);
        text.pop();

        let ct = encrypt_bytes(&text, &cipher, symm_alg, symm_len, &digest_fn, &their_pub);
        println!("{}", ct);

    // DECRYPT naredba
    } else if let Some(matches) = matches.subcommand_matches("decrypt") {
        let my_sec = read_rsa_seckey(matches);

        let message_path = matches.value_of("MSG").unwrap();
        let mut message = read_from_file(&message_path);
        message.pop();

        let (ct, key, iv) = parse_encrypted_message(&message);

        let ct = decode_block(&ct).expect("Unable to decode base64 block.");
        let pt = decrypt_bytes(&ct, &key, &iv, &my_sec, &cipher, symm_alg);
        let pt_str = String::from_utf8(pt).expect("Unable to convert to string.");
        println!("{}", pt_str);

    // SEAL naredba
    } else if let Some(matches) = matches.subcommand_matches("seal") {
        let their_pub = read_rsa_pubkey(matches);

        let message_path = matches.value_of("MSG").unwrap();
        let mut text = read_from_file(&message_path);
        text.pop();

        let ct = encrypt_bytes(&text, &cipher, symm_alg, symm_len, &digest_fn, &their_pub);

        let my_sec = read_rsa_seckey(matches);
        let my_key = PKey::from_rsa(my_sec).unwrap();
        let signature = sign_bytes(&ct.as_bytes(), &my_key, digest_fn);

        let msg = concat_message_signature(&ct.as_bytes(), &signature);
        println!("{}", msg);

    // UNSEAL naredba
    } else if let Some(matches) = matches.subcommand_matches("unseal") {
        let their_pub = read_rsa_pubkey(matches);
        let their_pub = PKey::from_rsa(their_pub).unwrap();

        let message_path = matches.value_of("MSG").unwrap();

        let text = read_from_file(&message_path);
        let (text, b64_sig) = parse_signed_message(&text);
        //println!("{:?}", text);
        //println!("{:?}", b64_sig);

        let signature_bytes = decode_block(&b64_sig).expect("Unable to decode signature bytes.");
        let valid = verify_signature(&text.as_bytes(), &their_pub, &signature_bytes, digest_fn);
        if !valid {
            println!("Verification failed.");
        } else {
            let my_sec = read_rsa_seckey(matches);
            //println!("{}", text);
            let (ct, key, iv) = parse_encrypted_message(&text.as_bytes());
            //println!("{:?}", &ct);
            //println!("{:?}", &key);
            //println!("{:?}", &iv);

            let ct = decode_block(&ct).unwrap();
            let pt = decrypt_bytes(&ct, &key, &iv, &my_sec, &cipher, symm_alg);
            println!("{}", &String::from_utf8(pt).unwrap());
        }
    }
}

fn encrypt_bytes(
    bytes: &[u8],
    cipher: &Cipher,
    _enc_alg: &str,
    symm_len: u32,
    digest_fn: &MessageDigest,
    their_pub: &Rsa<Public>,
) -> String {
    let mut sym_key = BigNum::new().unwrap();
    sym_key.rand(symm_len as i32, MsbOption::ONE, true).unwrap();
    let key_iv = bytes_to_key(
        cipher.clone(),
        digest_fn.clone(),
        &sym_key.to_vec(),
        None,
        symm_len as i32,
    )
    .expect("Unable to derive key from bits.");

    let mut key_buffer = [0; 256];
    let len = their_pub
        .public_encrypt(&key_iv.key, &mut key_buffer, Padding::PKCS1)
        .expect("Unable to encrypt session key using public key");
    let mut key_str = encode_block(&key_buffer[..len]);

    let ct = match key_iv.iv {
        Some(x) => {
            let iv_str = encode_block(&x);
            key_str.push(';');
            key_str.push_str(&iv_str);

            encrypt(cipher.clone(), &key_iv.key, Some(&x[..]), bytes)
                .expect("Unable to encrypt plaintext with nonce...")
        }
        None => encrypt(cipher.clone(), &key_iv.key, None, bytes)
            .expect("Unable to encrypt plaintext with no nonce..."),
    };

    let ct = encode_block(&ct);
    concat_message_sessionkey(&ct.as_bytes(), &key_str)
}

fn decrypt_bytes(
    bytes: &[u8],
    b64_key: &str,
    b64_iv: &Option<String>,
    my_sec: &Rsa<Private>,
    cipher: &Cipher,
    enc_alg: &str,
) -> Vec<u8> {
    let key_bytes = decode_block(b64_key).expect("Unable to decode session key from base64");
    let iv_bytes = match b64_iv {
        Some(x) => Some(decode_block(x).expect("Unable to decode session key from base64")),
        None => None,
    };

    let mut decrypted_key = [0; 256];
    let len = my_sec
        .private_decrypt(&key_bytes, &mut decrypted_key, Padding::PKCS1)
        .expect("Unable to decrypt session key using private key.");

    let pt = match iv_bytes {
        Some(x) => {
            let iv_len = match enc_alg {
                ALG_AES => 16,
                ALG_BF => 8,
                _ => panic!("Unsupported algorithm!"),
            };
            decrypt(
                cipher.clone(),
                &decrypted_key[..len],
                Some(&x[..iv_len]),
                bytes,
            )
            .expect("Unable to decrypt ciphertext.")
        }
        None => decrypt(cipher.clone(), &decrypted_key[..len], None, bytes)
            .expect("Unable to decrypt ciphertext."),
    };
    pt
}

fn sign_bytes(bytes: &[u8], key: &PKey<Private>, digest_fn: MessageDigest) -> String {
    let mut signer = Signer::new(digest_fn, &key).expect("Unable to construct the signer.");
    signer.update(&bytes).expect("Unable to update signer.");
    let signature = signer.sign_to_vec().expect("Unable to sign message.");
    encode_block(&signature)
}

fn verify_signature(
    bytes: &[u8],
    key: &PKey<Public>,
    signature: &[u8],
    digest_fn: MessageDigest,
) -> bool {
    let mut verifier = Verifier::new(digest_fn, key).expect("Unable to create verifier!");
    verifier
        .update(bytes)
        .expect("Unable to update verifier with bytes");
    verifier
        .verify(signature)
        .expect("Unable to verify signature!")
}

fn concat_message_sessionkey(bytes: &[u8], b64_session_key: &str) -> String {
    let mut msg = String::from_utf8(bytes.to_vec()).expect("Unable to convert message to utf-8");
    msg.push('\n');
    msg.push('\n');
    msg.push_str("-----BEGIN SESSION KEY-----\n");
    msg.push_str(b64_session_key);
    msg.push('\n');
    msg.push_str("-----END SESSION KEY-----");
    msg
}

fn concat_message_signature(bytes: &[u8], signature: &str) -> String {
    let mut msg = String::from_utf8(bytes.to_vec()).expect("Unable to convert message to utf-8");
    msg.push('\n');
    msg.push('\n');
    msg.push_str("-----BEGIN SIMPLE SIGNATURE-----\n");
    msg.push_str(signature);
    msg.push('\n');
    msg.push_str("-----END SIMPLE SIGNATURE-----");
    msg
}

fn parse_signed_message(bytes: &[u8]) -> (String, String) {
    let len = bytes.len();
    let whole_text = String::from_utf8(bytes.to_vec()).unwrap();
    if !whole_text.contains("-----BEGIN SIMPLE SIGNATURE-----") {
        panic!("File is not signed!");
    }

    let mut i = 1;
    let mut dash_counter = 0;
    let dash = '-' as u8;
    loop {
        if bytes[len - i] == dash {
            dash_counter += 1;
        }
        if dash_counter == 20 {
            i += 1;
            break;
        }
        i += 1;
    }

    let message = whole_text[..(len - i - 1)].to_string();
    let wrapped_sig = whole_text[(len - i + 1)..(len - 1)].to_string();
    let sig_len = wrapped_sig.len();
    //33 is length of '-----BEGIN SIMPLE SIGNATURE-----'
    //31 is length of '-----END SIMPLE SIGNATURE-----'
    let unwrapped_sig = wrapped_sig[33..(sig_len - 31)].to_string();
    (message, unwrapped_sig)
}

fn parse_encrypted_message(bytes: &[u8]) -> (String, String, Option<String>) {
    let len = bytes.len();
    let whole_text = String::from_utf8(bytes.to_vec()).unwrap();
    if !whole_text.contains("-----BEGIN SESSION KEY-----") {
        panic!("File is not encrypted!");
    }

    let mut i = 1;
    let mut dash_counter = 0;
    let dash = '-' as u8;
    loop {
        if bytes[len - i] == dash {
            dash_counter += 1;
        }
        if dash_counter == 20 {
            i += 1;
            break;
        }
        i += 1;
    }

    let message = whole_text[..(len - i - 1)].to_string();
    let wrapped_key = whole_text[(len - i + 1)..(len - 1)].to_string();
    let key_len = wrapped_key.len();
    //28 is length of '-----BEGIN SESSION KEY-----'
    //26 is length of '-----END SESSION KEY-----' - 1 for newline
    let unwrapped_key_iv = wrapped_key[28..(key_len - 25)].to_string();
    let parts: Vec<&str> = unwrapped_key_iv.split(';').collect();
    let unwrapped_key = String::from(parts[0]);
    let unwrapped_iv = if parts.len() > 1 {
        Some(String::from(parts[1]))
    } else {
        None
    };
    (message, unwrapped_key, unwrapped_iv)
}

fn read_rsa_seckey(matches: &ArgMatches) -> Rsa<Private> {
    let skey_path = matches.value_of("S_KEY").unwrap();
    let sk_pem = read_from_file(skey_path);

    let skey = Rsa::private_key_from_pem(&sk_pem)
        .expect("Unable to reconstruct private key from PKCS8 format.");
    skey
}

fn read_rsa_pubkey(matches: &ArgMatches) -> Rsa<Public> {
    let pkey_path = matches.value_of("P_KEY").unwrap();
    let pk_pem = read_from_file(pkey_path);

    let pkey =
        Rsa::public_key_from_pem(&pk_pem).expect("Unable to reconstruct public key from PEM.");
    pkey
}

fn generate_keys(bits: u32, skey: &str, pkey: &str) {
    let rsa = Rsa::generate(bits).unwrap();
    let key = PKey::from_rsa(rsa).unwrap();

    let pub_key: Vec<u8> = key.public_key_to_pem().unwrap();
    let sec_key: Vec<u8> = key.private_key_to_pem_pkcs8().unwrap();
    write_to_file(&sec_key, skey);
    write_to_file(&pub_key, pkey);
}

fn create_cipher(algo: &str, key_len: u32, mode: &str) -> Cipher {
    let key_len = key_len.to_string();

    if algo == ALG_AES && key_len == SYM_128 && mode == MODE_ECB {
        return Cipher::aes_128_ecb();
    } else if algo == ALG_AES && key_len == SYM_128 && mode == MODE_CBC {
        return Cipher::aes_128_cbc();
    } else if algo == ALG_AES && key_len == SYM_128 && mode == MODE_OFB {
        return Cipher::aes_128_ofb();
    } else if algo == ALG_AES && key_len == SYM_128 && mode == MODE_CTR {
        return Cipher::aes_128_ctr();
    } else if algo == ALG_AES && key_len == SYM_192 && mode == MODE_ECB {
        return Cipher::aes_192_ecb();
    } else if algo == ALG_AES && key_len == SYM_192 && mode == MODE_CBC {
        return Cipher::aes_192_cbc();
    } else if algo == ALG_AES && key_len == SYM_192 && mode == MODE_OFB {
        return Cipher::aes_192_ofb();
    } else if algo == ALG_AES && key_len == SYM_192 && mode == MODE_CTR {
        return Cipher::aes_192_ctr();
    } else if algo == ALG_AES && key_len == SYM_256 && mode == MODE_ECB {
        return Cipher::aes_256_ecb();
    } else if algo == ALG_AES && key_len == SYM_256 && mode == MODE_CBC {
        return Cipher::aes_256_cbc();
    } else if algo == ALG_AES && key_len == SYM_256 && mode == MODE_OFB {
        return Cipher::aes_256_ofb();
    } else if algo == ALG_AES && key_len == SYM_256 && mode == MODE_CTR {
        return Cipher::aes_256_ctr();
    } else if algo == ALG_BF && mode == MODE_ECB {
        return Cipher::bf_ecb();
    } else if algo == ALG_BF && mode == MODE_CBC {
        return Cipher::bf_cbc();
    } else if algo == ALG_BF && mode == MODE_OFB {
        return Cipher::bf_ofb();
    }

    panic!("Unsupported combination: {} {} {}", algo, key_len, mode);
}

fn parse_symm_alg<'a>(matches: &ArgMatches) -> &'a str {
    let sym_alg = matches.value_of("SYMM_ALG").unwrap_or(ALG_AES);
    let sym_alg = match sym_alg {
        ALG_AES => ALG_AES,
        ALG_BF => ALG_BF,
        _ => panic!("Unsupported symmetric algorithm option! Please use 'AES' or 'BF'."),
    };
    sym_alg
}

fn parse_symm_len(matches: &ArgMatches) -> u32 {
    let len = matches.value_of("SYMM_KEY").unwrap_or(SYM_256);
    let len = match len {
        SYM_128 => 128,
        SYM_192 => 192,
        SYM_256 => 256,
        _ => panic!("Unsupported symmetric key length. Please use 128, 192 or 256 bits"),
    };
    len
}

fn parse_symm_mode<'a>(matches: &ArgMatches) -> &'a str {
    let mode = matches.value_of("SYMM_MOD").unwrap_or(MODE_CBC);
    let mode = match mode {
        MODE_ECB => MODE_ECB,
        MODE_CBC => MODE_CBC,
        MODE_CFB => MODE_CFB,
        MODE_OFB => MODE_OFB,
        MODE_CTR => MODE_CTR,
        _ => {
            panic!("Unsupported symmetric encryption mode! Allowed modes: ECB, CBC, CFB, OFB, CTR")
        }
    };
    mode
}

fn parse_asym_key_len(matches: &ArgMatches) -> u32 {
    let asym_key_len = matches.value_of("ASYM_LEN").unwrap_or(RSA_2048);
    let asym_key_len = match asym_key_len {
        RSA_1024 => 1024,
        RSA_2048 => 2048,
        RSA_3072 => 3072,
        RSA_4096 => 4096,
        _ => panic!("Unsupported key length! Supported values are: 1024, 2048, 3072, 4096."),
    };
    asym_key_len
}

fn parse_hash_fun(matches: &ArgMatches) -> MessageDigest {
    let hash_fun = matches.value_of("HASH_FUN").unwrap_or(HASH_SHA3_512);
    let md = match hash_fun {
        HASH_MD5 => MessageDigest::md5(),
        HASH_SHA1 => MessageDigest::sha1(),
        HASH_SHA224 => MessageDigest::sha224(),
        HASH_SHA256 => MessageDigest::sha256(),
        HASH_SHA384 => MessageDigest::sha384(),
        HASH_SHA512 => MessageDigest::sha512(),
        HASH_SHA3_224 => MessageDigest::sha3_224(),
        HASH_SHA3_256 => MessageDigest::sha3_256(),
        HASH_SHA3_384 => MessageDigest::sha3_384(),
        HASH_SHA3_512 => MessageDigest::sha3_512(),
        _ => panic!("Unsupported hash function. Supported functions are: MD5, SHA1, SHA224, SHA256, SHA384, SHA512, SHA3_224, SHA3_256, SHA3_384, SHA3_512"),
    };
    md
}

fn write_to_file(bytes: &[u8], path: &str) {
    let mut file = File::create(path).expect("Unable to open file for writing.");
    file.write_all(bytes).expect("Unable to write to file.");
}

fn read_from_file(path: &str) -> Vec<u8> {
    let mut file = File::open(path).expect("Unable to open file for reading.");
    let mut buf = vec![];
    file.read_to_end(&mut buf)
        .expect("Unable to read from file.");
    buf
}
