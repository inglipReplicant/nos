extern crate os_pipe;

use std::collections::HashMap;
use std::cmp::Ordering;
use std::io::{Read, Write};
use std::string::ToString;

use os_pipe::{PipeReader, PipeWriter};

pub static ZAHTJEV: &str = "ZAHTJEV\n";
pub static ODGOVOR: &str = "ODGOVOR\n";
pub static IZLAZAK: &str = "IZLAZAK\n";

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Message {
    pub index: usize,
    pub timestamp: usize,
    pub msg: String,
}

impl Message {
    pub fn new(i: usize, t: usize, msg: &str) -> Message {
        let msg = String::from(msg);
        Message {
            index: i,
            timestamp: t,
            msg: msg,
        }
    }
}

impl Ord for Message {
    fn cmp(&self, other: &Message) -> Ordering {
        if self.timestamp.cmp(&other.timestamp) == Ordering::Equal {
            return self.index.cmp(&other.index).reverse();
        }
        self.timestamp.cmp(&other.timestamp).reverse()
    }
}

impl ToString for Message {
    fn to_string(&self) -> String {
        let mut res = String::new();
        res.push_str(&self.index.to_string());
        res.push(';');
        res.push_str(&self.timestamp.to_string());
        res.push(';');
        res.push_str(&self.msg);
        res.push(':');
        res
    }
}

impl PartialOrd for Message {
    fn partial_cmp(&self, other: &Message) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

pub struct IndexedPipe {
    pub their_id: usize,
    pub my_reader: PipeReader,
    pub my_writer: PipeWriter,
}

impl IndexedPipe {
    pub fn new(id: usize, my_r: PipeReader, my_w: PipeWriter) -> IndexedPipe {
        IndexedPipe {
            their_id: id,
            my_reader: my_r,
            my_writer: my_w,
        }
    }
}

pub struct Philosopher {
    pub id: usize,
    pub n: usize,
    pub pipes: HashMap<usize, IndexedPipe>,
}

impl Philosopher {
    pub fn send(&mut self, other_id: usize, msg: &str) -> () {
        println!("{}->{}: {:?}", self.id, other_id, msg);
        let ipipe = self.pipes.get_mut(&other_id).unwrap();
        ipipe.my_writer.write(&msg.as_bytes()).unwrap();
        ipipe.my_writer.flush().unwrap()
    }

    pub fn read(&mut self, other_id: usize) -> String {
        let ipipe = self.pipes.get_mut(&other_id).unwrap();
        let mut buf = [0; 512];
        let len = ipipe.my_reader.read(&mut buf).unwrap();
        let res = String::from_utf8(buf[..len].to_vec()).unwrap();
        println!("{}<-{}: {:?}", self.id, other_id, res);
        res
    }
}
